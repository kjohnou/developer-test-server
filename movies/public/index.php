<?php
/**
* Index.php
*
* This file routes requests and processes data accordingly
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/*Import Dependencies*/
require '../vendor/autoload.php';

/*Settings to display verbose error details*/
$settings =  [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

/*Set Slim App*/
$app = new \Slim\App($settings);


/*Route for request: /movies
   -Parameters: start number for pagination
   -Returns: JSON array of all movies
*/
$app->get('/movies', function (Request $request, Response $response, array $args) {
    $movieguide = new \App\Controller\Movieguide;
    $allGetVars = $request->getQueryParams();
    $movieArray = $movieguide->allMovies($allGetVars);
   
    return $response->withJson($movieArray,200,JSON_UNESCAPED_UNICODE);
});

/*Route for request: /movie_by_title
    -Parameters: title, start number for pagination
    -Returns: JSON array of movies based on title
*/
$app->get('/movies_by_title', function (Request $request, Response $response, array $args) {
    $movieguide = new \App\Controller\Movieguide;
    $allGetVars = $request->getQueryParams();
    $movieArray = $movieguide->getMoviesbyTitle($allGetVars);

    return $response->withJson($movieArray,200,JSON_UNESCAPED_UNICODE);
});

/*Route for request: /movie_by_category
    -Parameters: category, start number for pagination
    -Returns: JSON array of movies based on category id
*/
$app->get('/movies_by_category', function (Request $request, Response $response, array $args) {
    $movieguide = new \App\Controller\Movieguide;
    $allGetVars = $request->getQueryParams();
    $movieArray = $movieguide->getMoviesbyCategory($allGetVars);

    return $response->withJson($movieArray,200,JSON_UNESCAPED_UNICODE);
});

/*Route for request: /actors_by_movie
    -Parameters: film id, start number for pagination
    -Returns: JSON array of actors based on film id
*/
$app->get('/actors_by_movie', function (Request $request, Response $response, array $args) {
    $movieguide = new \App\Controller\Movieguide;
    $allGetVars = $request->getQueryParams();
    $movieArray = $movieguide->getActorsByMovie($allGetVars);

    return $response->withJson($movieArray,200,JSON_UNESCAPED_UNICODE);
});

/*Route for request: /movies_by_rating
    -Parameters: rating, start number for pagination
    -Returns: JSON array of movies based on rating
*/
$app->get('/movies_by_rating', function (Request $request, Response $response, array $args) {
    $movieguide = new \App\Controller\Movieguide;
    $allGetVars = $request->getQueryParams();
    $movieArray = $movieguide->getMoviesByRating($allGetVars);

    return $response->withJson($movieArray,200,JSON_UNESCAPED_UNICODE);
});

$app->run();
