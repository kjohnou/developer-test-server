<?php
/**
* Configuration File
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/

return [
        'database' => [
            'host' => 'mysql',
            'port' => '3306',
            'name' => 'sakila',
            'user' => 'sakila',
            'password' => 'sakila'
        ]

    ];
?>