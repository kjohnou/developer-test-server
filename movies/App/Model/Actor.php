<?php
 /**
* Actor Class
*
* This class is used for Actor Object.
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/

class Actor
{
  private $_actor_id;
  private $_first_name;
  private $_last_name;
   
  public function __construct(DBQueryResult $result){
    $this->_actor_id = $result->actor_id;
    $this->_first_name = $result->first_name;
    $this->_last_name = $result->last_name;
  }
  public function __get($var){
    switch ($var){
      case 'actor_id':
        return $this->_actor_id;
        break;
      case 'first_name':
        return $this->_first_name;
        break;
      case 'last_name':
        return $this->_last_name;
        break;
      default:
        return null;
        break;
    }
  }
  public function __toFullName(){
    return $this->_first_name.' '.$this->_last_name;
  }
}
 
?>