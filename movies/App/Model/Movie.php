<?php
 /**
* Movie Class
*
* This class is used for Movie Object.
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/

class Movie
{
  private $_film_id;
  private $_title;
  private $_description;
  private $_rating;
  private $_category;
   
  public function __construct(DBQueryResult $result){
    $this->_film_id = $result->film_id;
    $this->_title = $result->title;
    $this->_description = $result->description;
    $this->_rating = $result->rating;
    $this->_category = $result->category;
  }
  public function __get($var){
    switch ($var){
      case 'film_id':
        return $this->_film_id;
        break;
      case 'title':
        return $this->_title;
        break;
      case 'description':
        return $this->_description;
        break;
      case 'rating':
        return $this->_rating;
        break;
      case 'category':
        return $this->_category;
      default:
        return null;
        break;
    }
  }
}
 
?>