<?php
/**
* MovieGuide - DB Class
*
* This class provides connection to the movieguide database.
*
* @version    1.0
* @since      11-6-19
* @author     Kim John
*/

namespace App\Model;

/*Import Dependencies*/
require '../vendor/autoload.php';

use PDO;

class MovieDB{
    private $app;
    private $db;

    /*Constructor - Establish connection to database*/
    public function __construct(){
        try {
            /*TODO: Utilize Environment Variables for DB Credentials*/
            $database = $user = $password = "sakila";
            $host = "mysql"; 

            // assign PDO object to db variable
            $this->db = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $password);
            $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            //Output error - would normally log this to error file rather than output to user.
            die("Connection Error: " . $e->getMessage());
	    }
    }

    /*Pull all movie details from film_list view*/
    /*TODO: Return results as array of Movie objects*/
    public function getMovies($count_only = false, $offset = null, $no_of_records = null){
        $movies = null;
        try{
            if($count_only){
                $q = $this->db->prepare("SELECT COUNT(*) FROM film_list");
            }else{
                $q = $this->db->prepare("SELECT * FROM film_list ORDER BY FID LIMIT $offset, $no_of_records");
            }

            $q->execute();
            if(isset($q)){
                $movies = $q->fetchAll(PDO::FETCH_ASSOC);	
            }	
		}catch (PDOException $e){
			error_log("Error in getMovies: " . $e);
			die();
		}
		
		return $movies;	

    }

    /*Pull movie details filtered by Title*/
    /*TODO: Return results as array of Movie objects*/
    public function getMoviesByTitle($title, $count_only = false, $offset = null, $no_of_records = null ){
        try{
            if($count_only){
                $q = $this->db->prepare("SELECT COUNT(*) FROM film_list where LOWER(title) LIKE ? ORDER BY FID");
            }else{
                $q = $this->db->prepare("SELECT * FROM film_list where LOWER(title) LIKE ? ORDER BY FID LIMIT $offset, $no_of_records");
            }
            $q->bindParam(1, $title, PDO::PARAM_STR);
            $q->execute();
            if(isset($q)){
                $movies = $q->fetchAll(PDO::FETCH_ASSOC);	
            }	
		}catch (PDOException $e){
			error_log("Error in getMoviesByTitle: " . $e);
			die();
		}
		
		return $movies;	

    }

    /*Pull movie details filtered by Category*/
    /*TODO: Return results as array of Movie objects*/
    public function getMoviesByCategory($category, $count_only = false, $offset = null, $no_of_records = null ){
        try{
            if($count_only){
                $q = $this->db->prepare("SELECT COUNT(*) FROM film_list where lower(category) = ?");
            }else{
                $q = $this->db->prepare("SELECT * FROM film_list WHERE lower(category) = ? LIMIT $offset, $no_of_records");
            }
            $q->bindParam(1, $category, PDO::PARAM_STR);
            $q->execute();
            if(isset($q)){
                $movies = $q->fetchAll(PDO::FETCH_ASSOC);	
            }	
		}catch (PDOException $e){
			error_log("Error in getMoviesByCategory: " . $e);
			die();
		}
		
		return $movies;	

    }

    /*Pull movie details filtered by Rating*/
    /*TODO: Return results as array of Movie objects*/
    public function getMoviesByRating($rating, $count_only = false, $offset = null, $no_of_records = null ){
        try{
            if($count_only){
                $q = $this->db->prepare("SELECT COUNT(*) FROM film where lower(rating) = ?");
            }else{
                $q = $this->db->prepare("SELECT * FROM film WHERE lower(rating) = ? LIMIT $offset, $no_of_records");
            }
            $q->bindParam(1, $rating, PDO::PARAM_STR);
            $q->execute();
            if(isset($q)){
                $movies = $q->fetchAll(PDO::FETCH_ASSOC);	
            }	
		}catch (PDOException $e){
			error_log("Error in getMoviesByRating: " . $e);
			die();
		}
		
		return $movies;	

    }

    /*Pull actors for a given movie ID*/
    /*TODO: Return results as array of Actor objects*/
    public function getActorsByMovie($movie_id, $count_only = false, $offset = null, $no_of_records = null ){
        try{
            if($count_only){
                $q = $this->db->prepare("SELECT COUNT(*) FROM film_actor where film_id = ?");
            }else{
                $q = $this->db->prepare("SELECT actors FROM film_list WHERE FID = ? LIMIT $offset, $no_of_records");
            }
            $q->bindParam(1, $movie_id, PDO::PARAM_STR);
            $q->execute();
            if(isset($q)){
                $movies = $q->fetchAll(PDO::FETCH_ASSOC);	
            }	
		}catch (PDOException $e){
			error_log("Error in getActorsByMovie: " . $e);
			die();
		}
		
		return $movies;	

    }

    /*TODO: Create function to return category id lookup */
    public function getCategories(){
        //TODO: return category name, category id
    }

}//end class
 
?>
