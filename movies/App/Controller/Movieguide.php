<?php
/**
* MovieGuide Controller Class
*
* This class is used to interface with the database object to serve data requested through routes in index.php.
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/
namespace App\Controller;

use App\Util\Util;

/*Import Dependencies*/
require '../vendor/autoload.php';

class Movieguide
{
    private $_db;

    /*TODO: Store this constant in a config or environment file*/
    const _RECORDS_PER_PAGE = 20;

    public function __construct(){
        $this->_db = new \App\Model\MovieDB;    
    }


    /*Movies
    -Parameters: start number for pagination
    -Returns: JSON array of movies
    */
    public function allMovies($getVars){

        //Pagination
        $start = Util::getStartParam($getVars);
        $offset = Util::getOffset($start, self::_RECORDS_PER_PAGE);
        /*********************/

        /*parameters to database getMovies:
        -count only boolean - return only the total row count
        -offset int - start position of results
        -records per page int - number of records to return
        */

        $results = $this->_db->getMovies(false, $offset, self::_RECORDS_PER_PAGE);
        $total_rows = $this->_db->getMovies(true);
        $total = $total_rows[0]['COUNT(*)'];
        /**********************************/

        //Build JSON array
        $links_array = Util::buildLinksArray($start,$total,'/movies'.$category,self::_RECORDS_PER_PAGE);
        $json_array = ['_links' => $links_array,
                        'total_rows' => $total,
                        'limit' => self::_RECORDS_PER_PAGE,
                        'results' => $results];
        /*********************/

        return $json_array;
    }

    /*getMoviesByCategory
    -Parameters: category id, start number for pagination
    -Returns: JSON array of movies based on category id
    */
    public function getMoviesByCategory($getVars){

        //Category Filter
        if (isset($getVars['category'])){
            $category = $getVars['category'];
            $lookupCategory = strtolower(urldecode($category));
        } else {
            $category = '';
        }
        /***********************/

        //Pagination
        $start = Util::getStartParam($getVars);
        $offset = Util::getOffset($start, self::_RECORDS_PER_PAGE);
        /*********************/

        /*parameters to database getMoviesByCategory:
        -category id int
        -count only boolean - return only the total row count
        -offset int - start position of results
        -records per page int - number of records to return
        */
        $results = $this->_db->getMoviesByCategory($lookupCategory, false, $offset, self::_RECORDS_PER_PAGE);
        $total_rows = $this->_db->getMoviesByCategory($lookupCategory, true);
        $total = $total_rows[0]['COUNT(*)'];
        /**********************************/

        //Build JSON array
        $links_array = Util::buildLinksArray($start,$total,'/movies_by_category?category='.$category,self::_RECORDS_PER_PAGE);
        $json_array = ['_links' => $links_array,
                        'total_rows' => $total,
                        'limit' => self::_RECORDS_PER_PAGE,
                        'results' => $results];
        /*********************/

        return $json_array;
    }

    /*getMoviesByTitle
    -Parameters: title, start number for pagination
    -Returns: JSON array of movies based on title
    */
    public function getMoviesByTitle($getVars){
        
        //Title Filter
        if (isset($getVars['title'])){
            $title = $getVars['title'];
            $lookupTitle = "%".strtolower(urldecode($title))."%";
        } else {
            $lookupTitle = '';
        }
        /***********************/

        //Pagination
        $start = Util::getStartParam($getVars);
        $offset = Util::getOffset($start,self::_RECORDS_PER_PAGE);
        /**********************/

        /*parameters to database getMoviesByTitle:
        -title string
        -count only boolean - true -> return only the total row count, false -> return row count based on pagination setting
        -offset int - start position of results
        -records per page int - number of records to return
        */
        $results = $this->_db->getMoviesByTitle($lookupTitle, false, $offset, self::_RECORDS_PER_PAGE);
        $total_rows = $this->_db->getMoviesByTitle($lookupTitle, true);
        $total = $total_rows[0]['COUNT(*)'];
        /***********************/

        //Build JSON Array
        $links_array = Util::buildLinksArray($start,$total,'/movies_by_title?title='.$title,self::_RECORDS_PER_PAGE);
        $json_array = ['_links' => $links_array,
                        'total_rows' => $total,
                        'limit' => self::_RECORDS_PER_PAGE,
                        'results' => $results];
        /********************/

        return $json_array;
    }

    /*getMoviesByRating
    -Parameters: rating, start number for pagination
    -Returns: JSON array of movies based on rating
    */
    public function getMoviesByRating($getVars){
        
        //Rating Filter
        if (isset($getVars['rating'])){
            $rating = $getVars['rating'];
            $lookupRating = strtolower(urldecode($rating));
        } else {
            $lookupRating = '';
        }
        /***********************/

        //Pagination
        $start = Util::getStartParam($getVars);
        $offset = Util::getOffset($start,self::_RECORDS_PER_PAGE);
        /**********************/

        /*parameters to database getMoviesByRating:
        -rating string
        -count only boolean - true -> return only the total row count, false -> return row count based on pagination setting
        -offset int - start position of results
        -records per page int - number of records to return
        */
        $results = $this->_db->getMoviesByRating($lookupRating, false, $offset, self::_RECORDS_PER_PAGE);
        $total_rows = $this->_db->getMoviesByRating($lookupRating, true);
        $total = $total_rows[0]['COUNT(*)'];
        /***********************/

        //Build JSON Array
        $links_array = Util::buildLinksArray($start,$total,'/movies_by_rating?rating='.$rating,self::_RECORDS_PER_PAGE);
        $json_array = ['_links' => $links_array,
                        'total_rows' => $total,
                        'limit' => self::_RECORDS_PER_PAGE,
                        'results' => $results];
        /********************/

        return $json_array;
    }

    /*getActorsByMovie
    -Parameters: film id, start number for pagination
    -Returns: JSON array of actors based on film id
    */
    public function getActorsByMovie($getVars){

        //Movie Filter
        if (isset($getVars['film_id'])){
            $movie_id = $getVars['film_id'];
        } else {
            $movie_id = '';
        }
        /***********************/

        //Pagination
        $start = Util::getStartParam($getVars);
        $offset = Util::getOffset($start, self::_RECORDS_PER_PAGE);
        /*********************/

        /*parameters to database getActorsByMovie:
        -film id int
        -count only boolean - return only the total row count
        -offset int - start position of results
        -records per page int - number of records to return
        */
        $results = $this->_db->getActorsByMovie($movie_id, false, $offset, self::_RECORDS_PER_PAGE);
        $total_rows = $this->_db->getActorsByMovie($movie_id, true);
        $total = $total_rows[0]['COUNT(*)'];
        /**********************************/

        //Build JSON array
        $links_array = Util::buildLinksArray($start,$total,'/actors_by_movie?film_id='.$movie_id,self::_RECORDS_PER_PAGE);
        $json_array = ['_links' => $links_array,
                        'total_rows' => $total,
                        'limit' => self::_RECORDS_PER_PAGE,
                        'results' => $results];
        /*********************/

        return $json_array;
    }

    
}
?>