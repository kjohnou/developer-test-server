<?php
/**
* Utility Class
*
* @version  1.0
* @since    11-5-19
* @author   Kim John
*/

namespace App\Util;

/*Import Dependencies*/
require '../vendor/autoload.php';

Class Util{
    public static function getStartParam($getVars){
        //Pagination
        if (isset($getVars['start'])) {
            $start = $getVars['start'];
        } else {
            $start = 1;
        }
        return $start;
    }

    public static function getOffset($start,$records_per_page){

        $offset = ($start - 1) * $records_per_page;

        return $offset;
    }

    public static function buildLinksArray($start,$total_rows,$selfURL,$records_per_page){
        $next_start = $start + $records_per_page + 1;
        if ($next_start < $total_rows){
            $nextURL = $selfURL.'?start='.$next_start;
        }
        if ($nextURL != null){
            $linkArray = ['base' => 'localhost:3000',
                        'self' => 'localhost:3000'.$selfURL,
                        'next' => $nextURL];
        }else{
            $linkArray = ['base' => 'localhost:3000',
                        'self' => 'localhost:3000'.$selfURL];
        }
        

        return $linkArray;
    }
}
  
?>