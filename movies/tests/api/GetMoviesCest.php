<?php 

class GetMoviesCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function allMovies(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET('/movies');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    public function moviesByTitle(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET('/movies_by_title?title=airplane&start=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
    public function moviesByCategory(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET('/movies_by_category?category=action&start=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    public function moviesByRating(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET('/movies_by_rating?rating=g&start=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    public function actorsByMovie(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGET('/actors_by_movie?film_id=21&start=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

}
