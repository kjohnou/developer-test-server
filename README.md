# Ultimate Moviegoers Guide

## Overview
A RESTful API serving up the following information:
* A list of movies
  * The user should be able to search the movies by title
  * The user should be able to filter the movies by rating
  * The user should be able to filter the movies by category
* Movie details for each movie
* A list of actors in a movie

## Frameworks
* Slim
* Codeception

## Approach
The following design patterns were considered in this API:
* Model-View-Controller
* Dependency Injection
* Data Access Object

## Testing
API Testing scripts using Codeception.

## Additional Considerations
The following considerations would be made if more time were allotted to this API:
* Implement Rate-Limit restrictions and Caching of data
* Implement environment variable file for database credentials and other sensitive information
* Error Handling with custom exceptions for the different functions with email alert capability
* Movies By Category would be done using the category id instead of the category string
* Provide API for category list to retrieve the category ids
* Return database results as array of Movie class objects or Actor class objects
* Create, Update, and Delete functionality would be added for movies
* Ability to review/rate movies

## Challenges
Challenges I faced in developing this application was simply the learning curve of using a framework I have not previously used. The challenge came with ensuring the proper dependencies were loaded and available where needed.

## Installation

Launch the test code with the following commands (Assuming you have Docker and Composer installed.)

```
cd developer_test_server/movies
composer install
cd ../
docker-compose up -d
```

## Usage
* Created several routes specific to the type of data being searched in order to keep the purpose of each function focused and limit the number of query parameters available for each route.
    * Movies By Title
    * Movies By Rating
    * Movies By Category
    * Actors By Movie

**All Movie Details**
* Params: start (int) - starting position of returned records
* Base URL: http://localhost:3000/movies
* Example: http://localhost:3000/movies?start=1

**Movies by Title**
* Params: title (string), start (int)
* Base URL: http://localhost:3000/movies_by_title
* Example: http://localhost:3000/movies_by_title?title=airplane&start=1

**Movies by Rating**
* Params: rating (string), start (int)
* Valid Rating Param Values ['g','pg','pg-13','r','nc-17']
* Base URL: http://localhost:3000/movies_by_rating
* Example: http://localhost:3000/movies_by_rating?rating=pg&start=1

**Movies by Category**
* Params: category (string), start (int)
* Valid Category Param Values ['Action','Animation','Children','Classics','Comedy','Documentary','Drama','Family','Foreign',
        'Games','Horror','Music','New','Sci-Fi','Sports','Travel']
* Base URL: http://localhost:3000/movies_by_category
* Example: http://localhost:3000/movies_by_category?category=action&start=1

**Actors by Movie**
* Params: film_id (int), start (int)
* Base URL: http://localhost:3000/actors_by_movie
* Example: http://localhost:3000/actors_by_movie?film_id=21&start=1




